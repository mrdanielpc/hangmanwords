angular.module('wordApp', []).controller('MainController', function($scope, $http) {

	$scope.data = null;

	$scope.loadWords = function() {
		$http({
  			method: 'GET',
  			url: 'http://ubuntu4.javabog.dk:54163/api/words'
		}).then(function successCallback(response) {
	    	$scope.data = response;
	  	}, function errorCallback(response) {
	    	console.log(response);
	  	});
	}

  	$scope.create = function(name){
  		$http.post("http://ubuntu4.javabog.dk:54163/api/saveword", {"name": name})
  		.then(function(res){
			$scope.loadWords();
			$scope.name = "";
		}), function(res){
			console.log(res);
		};
  	}


  	$scope.delete = function(id){
  		$http.delete("http://ubuntu4.javabog.dk:54163/api/deleteword/" + id)
  		.then(function(res){
			$scope.loadWords();
		}), function(res){
			console.log(res);
		};
  	}

  	$scope.loadWords();

});