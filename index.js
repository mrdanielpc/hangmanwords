var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var app = express();

app.use(express.static('public'));
app.use(bodyParser.json());

// connect
mongoose.connect('mongodb://localhost/words');

var Word = mongoose.model('Word', { name: String });


app.get('/api/words', function(req, res){
	console.log("/api/words called");
	Word.find({}, {
		'_id': true,
		'name': true
	}, function(err, words) {
		if(err) 
			res.status(500).json({"error": "fail"});
		else
			res.status(200).json(words);
		res.end();
	});
});

app.post('/api/saveword', function(req, res) {
	//console.log(req.body);
	var word = Word({
		name: req.body.name
	});

	word.save(function(err) {
		if(err)
			res.status(500).json({"error": "dberro"});
		else
			res.status(200);
		res.end();
	})
})

app.delete('/api/deleteword/:id', function(req, res){
	console.log(req.params.id);
	Word.findOne({_id: req.params.id}, function(err, word){
		if(err)
			res.status(500).json({"error": "error finding"});
		else{
			console.log(JSON.stringify(word))
			word.remove(function(err){
				if(err)
					res.status(500).json({"error" : "error delete"});
				else
					res.status(200);
			});
		}
		res.end();
	});
})

//daniel port s154163
app.listen(54163, function () {
	console.log('Example app listening on port 3000!')
});

